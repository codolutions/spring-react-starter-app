# init db

DROP DATABASE IF EXISTS commentbox;
CREATE DATABASE commentbox;
GRANT ALL PRIVILEGES ON commentbox.* TO 'comments'@'%' IDENTIFIED BY 'comments';
