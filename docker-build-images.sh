#!/bin/bash

# Remove existing containers
docker rm starterapp_mysql_container
docker rm starterapp_container

# Build images
docker-compose -f docker-compose-dev.yml build

# Start DB
docker-compose -f docker-compose-dev.yml up -d starterapp_mysql
# Allow it some time to start
# Ideally we would probe the service to see if it has become available
echo "Waiting for MySQL container to start up..."
sleep 10

# Connect to MySQL docker container and initialize the database.
mysql --protocol tcp -h localhost -P 3306 -u root -pmysqlrootpass < ./docker/db-data/init_db.sh || exit 1

# Start the app container
docker-compose -f docker-compose-dev.yml up -d starterapp

